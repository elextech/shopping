var urllib = require('url');
var DependencyDao = require('../dao/dependencydao')();
var cookie = require('../dao/cookie');
var fs = require('fs');

var apis = function(app){

    var nation2 = ['ru', 'es', 'pl', 'ae', 'tr'];
    var dictionary_url = __dirname.replace(/\\/g,'/').replace('routes','') + 'config/dictionary.json';

    app.get('/:type', function(req, res){
        var type = req.params.type;
        var cookieHandle = cookie.getHandler(req, res);
        var nation = cookieHandle.get('nation') || req.headers['accept-language'].split(',')[0].split('-')[1].toLowerCase();
        if(nation2.indexOf(nation) == -1){
            nation = 'es';
        }
        fs.readFile(dictionary_url, function (err, dict) {
            res.render('index', { title: 'shopping', type: type, nation: nation, dictionary: JSON.parse(dict)[nation] });
        });
        console.log('ip:'+getClientIp(req)+'----nation:' + nation);
        console.log(req.headers['accept-language']);
    });

    function getClientIp(req) {
        return req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
    };

    app.get('/product/detail/:id', function(req, res){
        var cookieHandle = cookie.getHandler(req, res);
        var id = req.params.id;
        var nation = cookieHandle.get('nation') || req.headers['accept-language'].split(',')[0].split('-')[1].toLowerCase();
        if(nation2.indexOf(nation) == -1){
            nation = 'es';
        }
        var type = cookieHandle.get('product_type');
        var opt = {
            'id': id
        };
        fs.readFile(dictionary_url, function (err, dict) {
            DependencyDao.select(opt, function(data) {
                res.render('detail', { title: 'shopping', type: type, id: id, nation: nation, data: data[0], dictionary: JSON.parse(dict)[nation] });
            });
        });
    });

    //数据接口
    app.get('/api/data/product/list/:parity_cate/:start', function(req, res){
        var cookieHandle = cookie.getHandler(req, res);
        var nation = cookieHandle.get('nation') || req.headers['accept-language'].split(',')[0].split('-')[1].toLowerCase();
        if(nation2.indexOf(nation) == -1){
            nation = 'es';
        }
        var opt = {
            'start': req.params.start,
            'parity_cate': req.params.parity_cate,
            'nation': nation,
            'pagesize': 10
        };
        DependencyDao.selectList(opt, function(data) {
            var params = urllib.parse(req.url, true);
            if (params.query && params.query.jsoncallback) {
                var str =  params.query.jsoncallback + '(' + JSON.stringify(data) + ')'; //jsonp
                res.end(str);
            }
            else {
                res.end(JSON.stringify(data)); //普通的json
            }
        });
    });

    //数据接口 nav
    app.get('/api/data/nav/list/:num', function(req, res){
        var cookieHandle = cookie.getHandler(req, res);
        var nation = cookieHandle.get('nation') || req.headers['accept-language'].split(',')[0].split('-')[1].toLowerCase();
        if(nation2.indexOf(nation) == -1){
            nation = 'es';
        }
        var opt = {
            'nation': nation,
            'number': req.params.num //num=0是全部
        };
        DependencyDao.selectNav(opt, function(data) {
            var params = urllib.parse(req.url, true);
            if (params.query && params.query.jsoncallback) {
                var str =  params.query.jsoncallback + '(' + JSON.stringify(data) + ')'; //jsonp
                res.end(str);
            }
            else {
                res.end(JSON.stringify(data)); //普通的json
            }
        });
    });

};

module.exports.apilist = apis;

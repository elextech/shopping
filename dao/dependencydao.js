var mysql = require('mysql'),
    pool = require('./pool'),
    extend = require('../util').extend,
    EventEmitter = require('events').EventEmitter;

var dependencydao = (function(){

    function DependencyDao() {
        if (!(this instanceof DependencyDao))
            return new DependencyDao();
        EventEmitter.prototype.constructor.apply(this, arguments);
    };

    extend(DependencyDao, EventEmitter);

    DependencyDao.prototype.response = function(err, conn) {
        var self = this;
        if (err) {
            return function(sql, fn){
                console.log(err);
                conn && conn.release();
                self.emit('error', err);
                return;
            }
        } else {
            return function(sql, data, fn) {
                conn.query(sql, data, function(err, result) {
                    if (err) {
                        console.log(err);
                        conn && conn.release();
                        self.emit('error', err);
                        return;
                    }
                    conn && conn.release();
                    if (fn) {
                        fn(result);
                    } else {
                        self.emit('done', result);
                    }
                });
            };
        }
        return this;
    };

    DependencyDao.prototype.query = function(sql, data, fn) {
        var self = this;
        pool.getConnection(function(err, conn){
            self.response(err, conn)(sql, data, fn);
        });
        return this;
    };

    /**
     * @: 查询数据
     */
    DependencyDao.prototype.select = function (options, fn) {
        var sql = 'SELECT * FROM `kp_links` WHERE parity_cate IS NOT NULL AND is_effect = 1 AND price != 0 AND id=' + options.id;
        this.query(sql, [], fn);
    };

    /**
     * @: 查询数据 列表list
     */
    DependencyDao.prototype.selectList = function (options, fn) {
        var sql = 'SELECT * FROM `kp_links` WHERE ';
        if(options.nation) {
            sql += ' nation="' + options.nation + '" AND ';
        }
        if(options.parity_cate != 'home') {
            sql += ' parity_cate="' + options.parity_cate + '" AND ';
        }
        sql += ' parity_cate IS NOT NULL AND is_effect = 1 AND price != 0 limit ' + options.start + ',' + options.pagesize;
        this.query(sql, [], fn);
    };

    /**
     * @: 查询数据 nav
     */
    DependencyDao.prototype.selectNav = function (options, fn) {
        var sql = 'SELECT cid, ' + options.nation + ' as language FROM `kp_paritycate_lang` WHERE ' + options.nation + ' IS NOT NULL ';
        if(options.number != 0) {
            sql += ' limit ' + options.number;
        }
        this.query(sql, [], fn);
    };
    

    return DependencyDao;

})();

module.exports = dependencydao;
